@extends('layouts.master')

@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <div class="box-body">

                        <ul class='nav nav-tabs'>
                            <li class=''> <b>Primero:</b> Pedir PocketQuery de MisEncontrados</li></br></br>
                            <li class=''> <b>Segundo:</b> Importarla en GSAK, filtrar solo los encontrados por el geodesafiado entre jun-Ago y actualizarlos para cargar los 
                            FAVs (en a pocket no vienen). Después la exportamos de nuevo  desde GSAK.
                            Esto hace que se actualicen los fav a la fecha en la que actualizamos. No podemos saber cuántos favs había el día de encontrado.</br></br> </li>
                            <li class=''> <b>Tercero:</b> En la web, importar el GPX, escribir el nombre del geodesafiado TAL Y COMO APARECE EN LA WEB y pulsar DÁMELO TODO!.
                            Saldrá una tabla en pantalla con los datos de JUNIO, JULIO y AGOSTO de 2019 y los resultados de los 3 geodesafíos.</br></br></li>
                        </ul>

            </div><!-- /.box-body -->



            <div class="box-body">

            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Seleccione el fichero GPX</h3>
                    </div>   
                    <div class="box-header">  
                    {!!   Form::  open( array( 'id'=>'FormIF', 'name'=>'FormName', 'autocomplete'=>'off' , 'url' =>route('verano19'), 'method' => 'POST'  ) )  !!}

                        <label for="ficherogpx">Fichero GPX</label>
                        <input type="file" id="ficherogpx" name="ficherogpx">

                        <p class="help-block">Solo se admiten fichero GPX pasados por el GSAK.</p>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Comprobar</button>
                        </div>
                    {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-info">
                    <div class="box-header with-border ">
                        <h3 class="box-title">Resultados</h3>
                    </div> 
                    <div class="box-header">  
                    <?php
                                if ( $fichero ) {

                                    
                                                print_r("</br>Fichero tratado : ".$fichero);
                                                print_r("</br>");
                                                print_r("</br>");


                                                $nombreMeses=['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Diciembre'];
                                                foreach($meses as $keymeses => $mes){
                                                    print_r('</br><b>'.$nombreMeses[$keymeses-1].'</b>');
                                                    print_r("</br>");
                                                            print_r('&nbsp;Favoritos: '.$mes['favoritos']);
                                                            print_r("</br>");
                                                            print_r('&nbsp;Años Completos: '.(int)($mes['SumaAnos']/360));
                                                            print_r("</br>");
                                                            print_r('&nbsp;Atributos Distintos: '.(int)($mes['Atributos']));
                                                            print_r("</br>");
                                                }
                                    }


                    ?>   
                    </div>                 
                </div>
            </div>
            </div><!-- /.box-body -->

        </div><!-- /.col -->

    </div><!-- /.row -->

@endsection

@section('body_bottom')
    <!-- Select2 4.0.0
    <script src="{{ asset ("/bower_components/admin-lte/select2/js/select2.min.js") }}" type="text/javascript"></script> -->

    <!-- Select2 js
    @include('partials._body_bottom_select2_js_user_settings') -->
@endsection
