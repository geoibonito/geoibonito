<?php
// Documentación:
// Primero: Pedir PocketQuery de MisEncontrados
// Segundo: Importarla en GSAK, filtrar solo los encontrados por el geodesafiado entre jun-Ago y actualizarlos para cargar los 
// FAVs (en a pocket no vienen). Después la exportamos de nuevo  desde GSAK. 
// Esto hace que se actualicen los fav a la fecha en la que actualizamos. No podemos saber cuántos favs había el día de encontrado.
// Tercero: En la web, importar el GPX, escribir el nombre del geodesafiado TAL Y COMO APARECE EN LA WEB y pulsar DÁMELO TODO!.
// Saldrá una tabla en pantalla con los datos de JUNIO, JULIO y AGOSTO de 2019 y los resultados de los 3 geodesafíos.
///////////////

$fichero = 'despuesdejunio.gpx';
//$fichero = 'encontrados.gpx';
$geodesafiado = "Ibonito";

$atributosEncontrados = [];
$todosLosCaches = [];

$xml = simplexml_load_file($fichero);
//var_dump($xml->wpt);
$favoritos = 0;
foreach($xml->wpt as $x){
    //print_r("GC : ".$x->name."</br>");
    $gcCache = "'".$x->name."'";


    //saco los namespaces - atributos
    $namespaces = $x->getNameSpaces(true);
    
    // tributos
    $gsak = $x->children($namespaces['gsak']);
   // echo $gsak->wptExtension->FavPoints
    foreach($gsak->wptExtension as $extension){
        //echo $extension->Code."-".$extension->FavPoints."</br>";
        //$gcCacheGsak = "'".$extension->Code."'";
        $favActual = (int)$extension->FavPoints;
        $favoritos = $favoritos + $favActual;
    }

        $todosLosCaches['Favoritos'] = $favoritos ;


	$media = $x->children($namespaces['groundspeak']);
	//echo "El thumbnail es:" .$media->cache."<br>";

    foreach($media->cache->attributes as $atributos){
        //echo "<pre>";
        foreach($atributos as $atrib){

            $misatributos = $atrib->attributes();
            if (true ) {
                //print_r($misatributos['id']."->".$atrib."</br>");
                $todosLosCaches['Caches'][$gcCache]['Atributos'][] = "['".$misatributos['id']."'=>'".$atrib."']";


                if (isset($misatributos['id'])){
                   // if (!array_key_exists(trim($misatributos['id']),$atributosEncontrados)) $atributosEncontrados[trim($misatributos['id'])] = 0;
                    //if (!isset( $atributosEncontrados[$misatributos['id']])) $atributosEncontrados[trim($misatributos['id'])] = 0;
                    if (!isset( $atributosEncontrados[trim($misatributos['id'])])) $atributosEncontrados[trim($misatributos['id'])] = 1;
                    $numero = $atributosEncontrados[trim($misatributos['id'])];
                    if (!$numero) $atributosEncontrados[trim($misatributos['id'])] = 0;
                    $atributosEncontrados[trim($misatributos['id'])]= $atributosEncontrados[trim($misatributos['id'])] + 1;
                }
            } else {
                //print_r("[Sin Atributos]");
                $todosLosCaches['Caches'][$gcCache]['Atributos'][]="[Sin Atributos]";
            }     
            //echo "</pre>";
        }
    }

    foreach($media->cache->logs->log as $milog){
        if ($milog->finder==$geodesafiado && $milog->type=="Found it"){ // COMPROBAR attended!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            $mifecha = substr($milog->date,8,2)."-".substr($milog->date,5,2)."-".substr($milog->date,0,4);
            //print_r("</br>Fecha encontrado : ".$milog->finder." - ".$mifecha);
            $todosLosCaches['Caches'][$gcCache]['Fecha encontrado']="['".$milog->finder."' => '".$mifecha."']";
        }
    }

    //print_r("</br>".$media->cache->state);
    $todosLosCaches['Caches'][$gcCache]['Lugar']="['Comunidad autónoma' => '".$media->cache->state."']";

    //print_r('</br></br></br>');

}
$todosLosCaches['Atributos distintos'][]=$atributosEncontrados;

// Llamar a la vista pasándole los parámetros de entrada y el array con los resultados.




echo "<pre>";
var_dump($todosLosCaches);
echo "</pre>";


    die('fin');























?>