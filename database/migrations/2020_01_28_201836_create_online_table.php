<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOnlineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('online', function(Blueprint $table) {
            $table->increments('id')->unsigned()->index();
			$table->string('nombre', 60);
			$table->unsignedInteger('user_id');
			$table->date('ultimo_acceso')->default(false);
            $table->string('modulo',100)->default(''); 
            $table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(config('online'), function(Blueprint $table) {
            $table->dropColumn('nombre');
            $table->dropColumn('usuario_id');
            $table->dropColumn('ultimo_acceso');
            $table->dropColumn('modulo');
        });
    }
}
