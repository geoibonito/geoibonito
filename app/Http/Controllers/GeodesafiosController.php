<?php namespace App\Http\Controllers;

use App\Repositories\AuditRepository as Audit;
use App\Repositories\Criteria\Error\ErrorByCreatedDateDescending;
use App\Repositories\Criteria\Error\ErrorCreatedBefore;
use App\Repositories\Criteria\Error\ErrorsWithUsers;
use App\Repositories\ErrorRepository as Error;
use Auth;
use Illuminate\Contracts\Foundation\Application;
use Setting;
use DateTime;
use Illuminate\Http\Request;
use App\Clases\ClaseGpx;

class GeodesafiosController extends Controller {

    /**
     * @var Error
     */
    private $error;

    /**
     * @param Route $route
     * @param Permission $permission
     */
    public function __construct(Application $app, Audit $audit, Error $error)
    {
        parent::__construct($app, $audit);
        $this->error = $error;
        // Set default crumbtrail for controller.
        session(['crumbtrail.leaf' => 'error']);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function verano19(Request $request)
    { 
        // Audit::log(Auth::user()->id, trans('admin/error/general.audit-log.category'), trans('admin/error/general.audit-log.msg-index'));

        // $page_title = trans('admin/error/general.page.index.title');
        // $page_description = trans('admin/error/general.page.index.description');
        // $purge_retention = Setting::get('errors.purge_retention');

        // $lern_errors = $this->error->pushCriteria(new ErrorByCreatedDateDescending())->pushCriteria(new ErrorsWithUsers())->paginate(20);

        // return view('admin.errors.index', compact('lern_errors', 'purge_retention', 'page_title', 'page_description'));

        // Documentación:
        // Primero: Pedir PocketQuery de MisEncontrados
        // Segundo: Importarla en GSAK, filtrar solo los encontrados por el geodesafiado entre jun-Ago y actualizarlos para cargar los 
        // FAVs (en a pocket no vienen). Después la exportamos de nuevo  desde GSAK. 
        // Esto hace que se actualicen los fav a la fecha en la que actualizamos. No podemos saber cuántos favs había el día de encontrado.
        // Tercero: En la web, importar el GPX, escribir el nombre del geodesafiado TAL Y COMO APARECE EN LA WEB y pulsar DÁMELO TODO!.
        // Saldrá una tabla en pantalla con los datos de JUNIO, JULIO y AGOSTO de 2019 y los resultados de los 3 geodesafíos.
        ///////////////

        $fichero = 'verano19.gpx';
       //$fichero = 'despuesdejunio.gpx';
        //$fichero = 'encontrados.gpx';
       // $fichero = 'junio18.gpx';
       $fichero = '';
        $geodesafiado = "Ibonito";

        $atributosEncontrados = [];
        $atributosEncontrados['mes_6']=[];
        $atributosEncontrados['mes_7']=[];
        $atributosEncontrados['mes_8']=[];
        $todosLosCaches = [];
        $meses[6]['favoritos'] = 0;
        $meses[7]['favoritos'] = 0;
        $meses[8]['favoritos'] = 0;
        $meses[6]['SumaAnos'] = 0;
        $meses[7]['SumaAnos'] = 0;
        $meses[8]['SumaAnos'] = 0;
        $meses[6]['Atributos'] = 0;
        $meses[7]['Atributos'] = 0;
        $meses[8]['Atributos'] = 0;

        if(isset($request->ficherogpx) && $request->ficherogpx) {
             $fichero = $request->ficherogpx;
        } else {
            $page_title = 'Geodesafios del verano 2019';
            $page_description = "Documentación:</br></br>";
    
            return view('geodesafios.verano19', compact( 'meses', 'page_title', 'page_description','fichero'));
        }     


        $xml = simplexml_load_file($fichero);
        //var_dump($xml->wpt);
        $favoritos = 0;
        foreach($xml->wpt as $x){
            //print_r("GC : ".$x->name."</br>");
            $gcCache = "'".$x->name."'";
            $gcCachePublicado = "'".$x->time."'";


            //saco los namespaces - atributos
            $namespaces = $x->getNameSpaces(true);
            
            // Atributos
            $gsak = $x->children($namespaces['gsak']);
        // echo $gsak->wptExtension->FavPoints
            foreach($gsak->wptExtension as $extension){
                //echo $extension->Code."-".$extension->FavPoints."</br>";
                //$gcCacheGsak = "'".$extension->Code."'";
                if (isset($extension->Parent)) continue;

                $favActual = (int)$extension->FavPoints;
                $favoritos = $favoritos + $favActual;

                //Por mes
                $mesEncontrado = substr($extension->UserFound,6,1);
                $meses[$mesEncontrado]['favoritos'] += $favActual;
            }

                $todosLosCaches['Favoritos'] = $favoritos ;


            $namespaces = $x->getNameSpaces(true);

if (!isset($namespaces["groundspeak"])) continue;
            $media = $x->children($namespaces['groundspeak']);
            //echo "El thumbnail es:" .$media->cache."<br>";

            foreach($media->cache->attributes as $atributos){
                //echo "<pre>";
                foreach($atributos as $atrib){

                    $misatributos = $atrib->attributes();
                    if (true ) {
                        //print_r($misatributos['id']."->".$atrib."</br>");
                        $todosLosCaches['Caches'][$gcCache]['Atributos'][] = "['".$misatributos['id']."'=>'".$atrib."']";


                        if (isset($misatributos['id'])){



                        // if (!array_key_exists(trim($misatributos['id']),$atributosEncontrados)) $atributosEncontrados[trim($misatributos['id'])] = 0;
                            //if (!isset( $atributosEncontrados[$misatributos['id']])) $atributosEncontrados[trim($misatributos['id'])] = 0;
                            if (!isset( $atributosEncontrados[trim($misatributos['id'])])) $atributosEncontrados[trim($misatributos['id'])] = 1;
                            $numero = $atributosEncontrados[trim($misatributos['id'])];
                            if (!$numero) {
                                $atributosEncontrados[trim($misatributos['id'])] = 0;
                                $atributosEncontrados['mes_'.$mesEncontrado][trim($misatributos['id'])] = 0;
                            }
                            $atributosEncontrados[trim($misatributos['id'])]= $atributosEncontrados[trim($misatributos['id'])] + 1;
                            $atributosEncontrados['mes_'.$mesEncontrado][trim($misatributos['id'])]= $atributosEncontrados[trim($misatributos['id'])] + 1;
                        }
                    } else {
                        //print_r("[Sin Atributos]");
                        $todosLosCaches['Caches'][$gcCache]['Atributos'][]="[Sin Atributos]";
                    }     
                    //echo "</pre>";
                }
            }

            foreach($media->cache->logs->log as $milog){
                if ($milog->finder==$geodesafiado && $milog->type=="Found it"){ // COMPROBAR attended!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    $mifecha = substr($milog->date,8,2)."-".substr($milog->date,5,2)."-".substr($milog->date,0,4);
                    //print_r("</br>Fecha encontrado : ".$milog->finder." - ".$mifecha);
                    $todosLosCaches['Caches'][$gcCache]['Fecha encontrado']="['".$milog->finder."' => '".$mifecha."']";

                    //Por mes
                    $mesEncontrado = substr($milog->date,6,1);

                    // echo "</br>-----------------------------------</br>";

                    // echo "</br>Cache: ".$gcCache;
                    // echo "</br>Publicado: ".substr($gcCachePublicado,1,10);
                    // echo "</br>Encontrado: ".substr($milog->date,0,10); 

                    $date1 = new DateTime(substr($milog->date,0,10));
                    $date2 = new DateTime(substr($gcCachePublicado,1,10));
                    $diff = $date1->diff($date2);
                    // this will output 4 days   
                    $diferenciaEnDias = $diff->days;                        

                    $tiempo = 10; //dias
                    $meses[$mesEncontrado]['SumaAnos'] += $diferenciaEnDias;
                }
            }

            //print_r("</br>".$media->cache->state);
            $todosLosCaches['Caches'][$gcCache]['Lugar']="['Comunidad autónoma' => '".$media->cache->state."']";

            //print_r('</br></br></br>');

        }
        $todosLosCaches['Atributos distintos'][]=$atributosEncontrados;


        $meses[$mesEncontrado]['Atributos'] = count($todosLosCaches["Atributos distintos"][0]['mes_'.$mesEncontrado]);
       // dd(count($todosLosCaches["Atributos distintos"][0]['mes_6']));
        // Llamar a la vista pasándole los parámetros de entrada y el array con los resultados.

        $page_title = 'Geodesafios del verano 2019';
        $page_description = "Documentación:</br></br>";

        return view('geodesafios.verano19', compact( 'meses', 'page_title', 'page_description','fichero'));

    }



    public function show($id)
    {
        $error = $this->error->find($id);

        Audit::log(Auth::user()->id, trans('admin/error/general.audit-log.category'), trans('admin/error/general.audit-log.msg-show'));

        $errorData = urldecode(http_build_query($error->data, '', PHP_EOL));

        $page_title = trans('admin/error/general.page.show.title');
        $page_description = trans('admin/error/general.page.show.description', ['error_id' => $error->id]);

        return view('admin.errors.show', compact('error', 'errorData', 'page_title', 'page_description'));
    }

    public function guardaGpx(){
        $gpx = New ClaseGpx();
        dd($gpx->guardaEnBdd());
    }

}