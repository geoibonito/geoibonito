<?php namespace App\Http\Controllers;


use Illuminate\Contracts\Foundation\Application;
use App\Repositories\AuditRepository as Audit;
use App\Models\Online;
use App\Models\Juego;
use App\Models\Geocacher;
use App\Models\fase;
use App\Models\tareasxfase;
use App\Models\respxtareaxgeocacher;
use App\Models\solucionesxgeocacher;use Carbon\Carbon;
use App\Clases\ClaseGeneraPartida;
use DB;
use Illuminate\Http\Request;

class MysteryController extends Controller
{
    /**
     * @param Application $app
     * @param Audit $audit
     */
    public function __construct(Application $app, Audit $audit)
    {
        parent::__construct($app, $audit);
        // Set default crumbtrail for controller.
        session(['crumbtrail.leaf' => 'mystery']);
    }


    public function index() {
        $data['tasks'] = [
            [
                'name' => 'Design New Dashboard',
                'progress' => '87',
                'color' => 'danger'
            ],
            [
                'name' => 'Create Home Page',
                'progress' => '76',
                'color' => 'warning'
            ],
            [
                'name' => 'Some Other Task',
                'progress' => '32',
                'color' => 'success'
            ],
            [
                'name' => 'Start Building Website',
                'progress' => '56',
                'color' => 'info'
            ],
            [
                'name' => 'Develop an Awesome Algorithm',
                'progress' => '10',
                'color' => 'success'
            ],
            [
                'name' => 'Analyse data',
                'progress' => '37',
                'color' => 'warning'
            ],
        ];

        $juego = 1;
        $tarea = $this->queMeToca($juego);



        //$usuariosOnline = $this->usuariosOnline();
        $page_title = "Mystery";
        $page_description = "Descripción de la Página";
        $ahora = Carbon::now()->subMinutes(5)->format('Y-m-d H:i:s');
        $usuariosOnline = Online::where('updated_at','>=',$ahora)->orderBy('updated_at','desc')->get()->toArray();

        if(!$tarea) dd("No hay más tareas activas de momento!!!!");
        return view('geocaching/'.$tarea[0]->url_tarea, compact('page_title', 'page_description'))->with($data)->with('usuariosOnline',$usuariosOnline)->with('ahora',$ahora);
        ////return view('geocaching/mystery', compact('page_title', 'page_description'))->with($data)->with('usuariosOnline',$usuariosOnline)->with('ahora',$ahora);
    }

    function usuariosOnline(){
        $ahora = Carbon::now()->subMinutes(5)->format('Y-m-d H:i:s');
        $usuariosOnline = Online::where('updated_at','>=',$ahora)->orderBy('updated_at','desc')->get()->toArray();
        return $usuariosOnline;
    }

    function queMeToca($juego=0){
        if ($juego){
            $userId = \Auth::user()->id;
            $miJuego = Juego::where('id','=',$juego)->first();
            if ($miJuego){
                $query = "
                select tareasxfase.url_tarea,solucionesxgeocacher.* from solucionesxgeocacher
                    left join tareasxfase on tareasxfase.id = solucionesxgeocacher.tarea_id
                    left join fases on fases.id = tareasxfase.fase_id

                    where tareasxfase.activo
                    and fases.juego_id = $juego
                    and fases.activo = 1
                    and solucionesxgeocacher.user_id = $userId
                    and isnull(solucionesxgeocacher.acertado)
                    order by tareasxfase.orden  limit 1";

                    $tareaDelUsuario = DB::select($query); 
                    return $tareaDelUsuario;
            }
        }
    }


function compruebarespuesta(request $request){

    $tareaQueMeToca = $this->queMeToca(1); 
    $solucionesxgeocacher = solucionesxgeocacher::where('id','=',$tareaQueMeToca[0]->id)->first();

    if(strtoupper($request->respuesta) == strtoupper($tareaQueMeToca[0]->solucion)){
        if ($solucionesxgeocacher){
            $solucionesxgeocacher->update([
                'acertado'=>1,
            ]);
            respxtareaxgeocacher::create([
                'solucionxgeocacher_id'=>$tareaQueMeToca[0]->id,
                'respuesta'=>$request->respuesta,
            ]);
        } else {

            dd("no encontrado!!. que raro!!");
        }
    } else {

            if ($solucionesxgeocacher){
                respxtareaxgeocacher::create([
                    'solucionxgeocacher_id'=>$tareaQueMeToca[0]->id,
                    'respuesta'=>$request->respuesta,
                ]);
            }


    }
    dd("Muy bien!!! has acertado!!!");
}

}
