<?php namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Clases\ClaseScrap;
use App\Models\Geocacher;
use App\Clases\ClaseGeneraPartida;

class EntradaMysteryController extends Controller
{

    public function index()
    {
        return view('geocaching/entradaMystery');
    }

    public function compruebaidentidad(Request $request )
    {
        $scrap = New ClaseScrap;

//        $todos = Geocacher::get();
//        $tiempo_inicial = microtime(true); //true es para que sea calculado en segundos
//         $contador = 0;
//         foreach($todos as $geocacher){
//             $cliente = $scrap->localizaGeocacher($geocacher->alias);
//             $scrap->guardaDatosDeGeocacher($cliente,$geocacher->alias);
//             $contador = $contador+1;
//             //if($contador >3) break;
//         }
//         $tiempo_final = microtime(true); //true es para que sea calculado en segundos
//         $tiempo_total =  $tiempo_final-$tiempo_inicial;
//         echo "El tiempo de ejecución del archivo ha sido de " . $tiempo_total . " segundos";

//     echo '
//     <table style="width:100%" border=1>
//     <tr>
//     <th style="width:15px">Num</th>
//     <th style="width:15px">Geocacher</th>
//     <th style="width:50px">Finds</th>
//       <th style="width:50px">Hides</th> 
//       <th style="width:50px">Favoritos</th>
//       <th style="width:50px">Ciudad</th>
//     </tr>';
// foreach($scrap->todosLosDatos as $registro){

//     echo '
//     <tr>
//     <td style="width:15px;">'.$contador.'</td>
//     <td style="width:50px;">'.trim($registro['geocacher']).'</td>
//     <td style="width:50px;">'.trim($registro['finds']).'</td>
//      <td style="width:50px;">'.trim($registro['hides']).'</td> 
//      <td style="width:50px;">'.trim($registro['favoritos']).'</td>
//      <td style="width:50px;">'.trim($registro['ciudad']).'</td>
//     </tr>';
// }
//      echo ' 
//   </table>    ';


// dd($scrap->todosLosDatos);


        $juego = 1;
        //$generador = new ClaseGeneraPartida();
        //$generador->generaParaElUsuarioActual($juego);

        $cliente = $scrap->localizaGeocacher($request->nick);
        $usuario = $scrap->guardaDatosDeGeocacher($cliente,$request->nick);
        $usuarioNuevo = isset($usuario['password']);
        if ($usuarioNuevo) {
            $scrap->enviaEmail($cliente,$texto="Toma tu clave de acceso: ".$usuario['password'],$request->nick);
            $page_title = "Datos de accceso enviados";
            $generador = new ClaseGeneraPartida;
            $generador->generaParaElUsuarioActual($juego,$usuario['user_id']);
            return view('geocaching.datosDeAccesoEnviados', compact('page_title'));
        } else {
            $page_title = "Identificate";
            return view('auth.logingeocodigodavinci', compact('page_title'));
        }

     }

}
