<?php 


        $fichero = 'Mis_Hidden.gpx';
        $geodesafiado = "Ibonito";



        $todo = [];

        $xml = simplexml_load_file($fichero);

        // Recorro los caches
        foreach($xml->wpt as $x){
            $gcCache = "'".$x->name."'";
            $gcCachePublicado = "'".$x->time."'";

            $todo[$gcCache]['Publicado']= $gcCachePublicado ;

            //saco los namespaces
            $namespaces = $x->getNameSpaces(true);
            $gsak = $x->children($namespaces['gsak']);
        
            // punto los favoritos
            foreach($gsak->wptExtension as $extension){
                if (isset($extension->Parent)) continue;

                $favActual = (int)$extension->FavPoints;
                $todo[$gcCache]['Favoritos']= $favActual ;
            }

            $namespaces = $x->getNameSpaces(true);
            if (!isset($namespaces["groundspeak"])) continue;
            $media = $x->children($namespaces['groundspeak']);

            // Apunto las propiedades del caché
            foreach ($media as $propiedad){
                $todo[$gcCache]['name'            ]=   $propiedad->name;
                $todo[$gcCache]['placed_by'       ]=   $propiedad->placed_by;
                $todo[$gcCache]['type'            ]=   $propiedad->type;
                $todo[$gcCache]['cache id'        ]=   $propiedad->cache;
                $todo[$gcCache]['difficulty'      ]=   $propiedad->difficulty;
                $todo[$gcCache]['terrain'         ]=   $propiedad->terrain;
                $todo[$gcCache]['country'         ]=   $propiedad->country;
                $todo[$gcCache]['state'           ]=   $propiedad->state;
                $todo[$gcCache]['owner id'        ]=   $propiedad->owner;
                $todo[$gcCache]['encoded_hints'   ]=   $propiedad->encoded_hints;
            }

            // Apunto los LOGS
            foreach($media->cache->logs->log as $milog){

                $mifecha = substr($milog->date,8,2)."-".substr($milog->date,5,2)."-".substr($milog->date,0,4);
                // $todo[$gcCache]['Logs'][]= "[   'geocacher' => $milog->finder,
                //                                 'type' => $milog->type,
                //                                 'text id' => $milog->text,
                //                                 'owner id' => $milog->finder,
                //                                 'date' => $mifecha,
                // ]" ;
                $todo[$gcCache]['Logs'][]= [   'geocacher' => $milog->finder,
                                                'type' => $milog->type,
                                                'text id' => $milog->text,
                                                'owner id' => $milog->finder,
                                                'date' => $mifecha,
                ] ;
            }


        $page_title = 'Importacion GPX';
        $page_description = "Documentación:</br></br>";
        // echo "<br>"."<br>";
        // echo "<pre>";
        // var_dump($todo);
        // echo "</pre>";

        foreach($todo as $key => $valor){
            echo "<b>".$key ."</b></br>";

            foreach($valor as $key2=>$valor2)
            if($key2 == 'Logs'){
                echo "&nbsp&nbsp".$key2 ."</br>";
                foreach($valor2 as $keylog=>$valorlog){ 
                    echo "&nbsp&nbsp&nbsp&nbsp".$keylog.".....".$valorlog['geocacher']."&nbsp&nbsp&nbsp&nbsp".$valorlog['type']."&nbsp&nbsp&nbsp&nbsp".$valorlog['date'] ."</br>";
                }
            } else {
                echo $key2.".....".$valor2 ."</br>";
            }
        }

        echo "</br>";
    }
