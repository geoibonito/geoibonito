<?php
/**
 * Created by PhpStorm.
 * User: ARL
 * Date: 06/03/19
 * Time: 12:33
 */

namespace App\Clases;
use App\Models\Cache;
use App\Models\Log;
use Datetime;

class ClaseGpx
{

    public function __construct( )
    {
    }

    public function guardaEnBdd(){


            $fichero = 'Mis_Hidden.gpx';
            $geodesafiado = "Ibonito";
            



            $todo = [];

            $xml = simplexml_load_file($fichero);

            // Recorro los caches
            foreach($xml->wpt as $x){
                $gcCache = "'".$x->name."'";
                $gcCachePublicado = "'".$x->time."'";

                $todo[$gcCache]['Publicado']= $gcCachePublicado ;

                //saco los namespaces
                $namespaces = $x->getNameSpaces(true);
                $gsak = $x->children($namespaces['gsak']);
            
                // punto los favoritos
                foreach($gsak->wptExtension as $extension){
                    if (isset($extension->Parent)) continue;

                    $favActual = (int)$extension->FavPoints;
                    $todo[$gcCache]['Favoritos']= $favActual ;
                }

                $namespaces = $x->getNameSpaces(true);
                if (!isset($namespaces["groundspeak"])) continue;
                $media = $x->children($namespaces['groundspeak']);

                // Apunto las propiedades del caché
                foreach ($media as $propiedad){
                    $todo[$gcCache]['name'            ]=   $propiedad->name;
                    $todo[$gcCache]['placed_by'       ]=   $propiedad->placed_by;
                    $todo[$gcCache]['type'            ]=   $propiedad->type;
                    $todo[$gcCache]['cache id'        ]=   $propiedad->cache;
                    $todo[$gcCache]['difficulty'      ]=   $propiedad->difficulty;
                    $todo[$gcCache]['terrain'         ]=   $propiedad->terrain;
                    $todo[$gcCache]['country'         ]=   $propiedad->country;
                    $todo[$gcCache]['state'           ]=   $propiedad->state;
                    $todo[$gcCache]['owner id'        ]=   $propiedad->owner;
                    $todo[$gcCache]['encoded_hints'   ]=   $propiedad->encoded_hints;
                }
                $publicado = new DateTime(substr($todo[$gcCache]['Publicado'       ],1,10));
                $existe = Cache::where('gc',$gcCache)->first();
                
                if(!$existe){
                    $cache = Cache::create([
                        'gc'            => $gcCache,
                        'Publicado'     => $publicado,
                        'Favoritos'     => $todo[$gcCache]['Favoritos'       ],
                        'name'          => $todo[$gcCache]['name'            ],
                        'placed_by'     => $todo[$gcCache]['placed_by'       ],
                        'type'          => $todo[$gcCache]['type'            ],
                        'cache_id'      => $todo[$gcCache]['cache id'        ],
                        'difficulty'    => $todo[$gcCache]['difficulty'      ],
                        'terrain'       => $todo[$gcCache]['terrain'         ],
                        'country'       => $todo[$gcCache]['country'         ],
                        'state'         => $todo[$gcCache]['state'           ],
                        'owner_id'      => $todo[$gcCache]['owner id'        ],
                        'encoded_hints' => $todo[$gcCache]['encoded_hints'   ],
                    ] );
                } else {
                    $cache = $existe->update([
                        'gc'            => $gcCache,
                        'Publicado'     => $publicado,
                        'Favoritos'     => $todo[$gcCache]['Favoritos'       ],
                        'name'          => $todo[$gcCache]['name'            ],
                        'placed_by'     => $todo[$gcCache]['placed_by'       ],
                        'type'          => $todo[$gcCache]['type'            ],
                        'cache_id'      => $todo[$gcCache]['cache id'        ],
                        'difficulty'    => $todo[$gcCache]['difficulty'      ],
                        'terrain'       => $todo[$gcCache]['terrain'         ],
                        'country'       => $todo[$gcCache]['country'         ],
                        'state'         => $todo[$gcCache]['state'           ],
                        'owner_id'      => $todo[$gcCache]['owner id'        ],
                        'encoded_hints' => $todo[$gcCache]['encoded_hints'   ],
                    ] ); 
                    Log::where('cache_id',$existe->id)->delete();
                    $cache = $existe;
                }

                // Apunto los LOGS
                foreach($media->cache->logs->log as $milog){

                    $mifecha = substr($milog->date,8,2)."-".substr($milog->date,5,2)."-".substr($milog->date,0,4);
                     $todo[$gcCache]['Logs'][]= [   'geocacher'     => $milog->finder,
                                                    'type'          => $milog->type,
                                                    'text id'       => $milog->text,
                                                    'owner id'      => $milog->finder,
                                                    'date'          => $mifecha,
                    ] ;

                    $log = Log::create([
                        'cache_id'     => $cache->id,
                        'geocacher'     => $milog->finder,
                        'type'          => $milog->type,
                         'text_id'       => $milog->text,
                         'owner_id'      => $milog->finder,
                         'date'          => $mifecha,
                     ] );
    
                }


            $page_title = 'Importacion GPX';
            $page_description = "Documentación:</br></br>";
            // echo "<br>"."<br>";
            // echo "<pre>";
            // var_dump($todo);
            // echo "</pre>";

            foreach($todo as $key => $valor){
                echo "<b>".$key ."</b></br>";

                foreach($valor as $key2=>$valor2)
                if($key2 == 'Logs'){
                    echo "&nbsp&nbsp".$key2 ."</br>";
                    foreach($valor2 as $keylog=>$valorlog){ 
                        echo "&nbsp&nbsp&nbsp&nbsp".$keylog.".....".$valorlog['geocacher']."&nbsp&nbsp&nbsp&nbsp".$valorlog['type']."&nbsp&nbsp&nbsp&nbsp".$valorlog['date'] ."</br>";
                    }
                } else {
                    echo $key2.".....".$valor2 ."</br>";
                }
            }

            echo "</br>";
        }

    }

}