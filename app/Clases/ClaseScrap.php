<?php namespace App\Clases;
use Goutte\Client;

use App\Models\Cache;
use App\Models\Log;
use App\Models\Geocacher;
use App\User;
use App\Http\Controllers;
use Datetime;
 

class ClaseScrap
{
  private $urlPerfil;
  public $todosLosDatos;
  private $miCliente;

function urlDeGeocaching($tipo="",$geocacher=""){

    $geocacher = $geocacher;
    $usernamelist = $tipo;
    if ($usernamelist == "about") {
      $urltoreturn = "https://www.geocaching.com/p/default.aspx?u=" . $geocacher;
    } else if ($usernamelist == "email") {
      $urltoreturn = "https://www.geocaching.com/email/?u=" . $geocacher;
    } else if ($usernamelist == "geocaches") {
      $urltoreturn = "https://www.geocaching.com/p/default.aspx?u=" . $geocacher . "&tab=geocaches#profilepanel";
    } else if ($usernamelist == "hidden") {
      $urltoreturn = "https://www.geocaching.com/seek/nearest.aspx?u=" . $geocacher;
    } else if ($usernamelist == "found") {
      $urltoreturn = "https://www.geocaching.com/seek/nearest.aspx?ul=" . $geocacher;
    } else if ($usernamelist == "trackables") {
      $urltoreturn = "https://www.geocaching.com/p/default.aspx?u=" . $geocacher . "&tab=trackables#profilepanel";
    } else if ($usernamelist == "souvenirs") {
      $urltoreturn = "https://www.geocaching.com/p/default.aspx?u=" . $geocacher . "&tab=souvenirs#profilepanel";
    } else if ($usernamelist == "gallery") {
      $urltoreturn = "https://www.geocaching.com/p/default.aspx?u=" . $geocacher . "&tab=gallery#profilepanel";
    } else if ($usernamelist == "lists") {
      $urltoreturn = "https://www.geocaching.com/p/default.aspx?u=" . $geocacher . "&tab=lists#profilepanel";
    } else if ($usernamelist == "stats") {
      $urltoreturn = "https://www.geocaching.com/p/default.aspx?u=" . $geocacher . "&tab=stats#profilepanel";
    } else if ($usernamelist == "projectgcstats") {
      $urltoreturn = "https://project-gc.com/ProfileStats/" . $geocacher;
    }
  
    if (!$geocacher) {
      return False;
    } else {
      return $urltoreturn;
    }

  
}


    function localizaGeocacher($geocacher=""){
            $this->urlPerfil = $this->urlDeGeocaching('about',urlencode ($geocacher));
            $encontradosHidesFavoritos = "";
            $client = new Client();
       
            // me logueo
            $crawler = $client->request('GET', 'https://www.geocaching.com/account/signin?returnUrl=%2fplay');
            $form = $crawler->selectButton('SignIn')->form();
            $crawler = $client->submit($form, array('UsernameOrEmail' => 'Ibonito', 'Password' => 'Rg182403'));
            return $client;
    }

function guardaDatosDeGeocacher($client,$geocacher=""){
            
            // Saco las estadísticas castilla+tere
           // $paginaPerfil = $client->request('GET', $this->urlPerfil);
$this->miCliente = $client;

            $paginaPerfil = $client->request('GET', $this->urlPerfil);
            $estadisticas =  $paginaPerfil->filter('div[class="profile-stats"]') ;  
            $encontradosHidesFavoritos="";
            foreach ($estadisticas as $domElement) {
                $encontradosHidesFavoritos = ($domElement->textContent);
            } ;

            //dd(array_filter ( explode(" ",preg_replace("/\r|\n/", "", $encontradosHidesFavoritos))));
            foreach( array_filter ( explode(" ",preg_replace("/\r|\n/", "", $encontradosHidesFavoritos))) as $key => $dato){
              $datos[] = $dato;
            }


           // if(count($datos)==4);
            // 0 => "3,374"
            // 1 => "finds"
            // 2 => "83"
            // 3 => "hides"
            // 4 => "117"
            // 5 => "Favorite"
            // 6 => "points"
            // 7 => "Barakaldo"
            if (isset($datos) && count($datos)>4){
                        $datosExtraidos['geocacher']  = $geocacher;
                        $datosExtraidos['finds']      = ($datos[0] == 'finds')  ? 0 : str_replace(',','',$datos[0]);
                        $datosExtraidos['hides']      = ($datos[0] == 'finds')  ? str_replace(',','',$datos[1]) : str_replace(',','',$datos[2]);
                        $datosExtraidos['favoritos']  = ($datos[4] != 'Favorite' ) ?  str_replace(',','',$datos[4]) : '';
                        $datosExtraidos['ciudad']     = (isset($datos[7])) ? $datos[7] : "";
            } else {
              $datosExtraidos['geocacher']  = $geocacher;
              $datosExtraidos['finds']      = '-';
              $datosExtraidos['hides']      = '-';
              $datosExtraidos['favoritos']  = '-';
              $datosExtraidos['ciudad']     = '-';
            }
            $this->todosLosDatos[] = $datosExtraidos;

          //dd($this->todosLosDatos);  
            $regGeocacher = Geocacher::where('alias','=',trim($geocacher))->first();//dd($regGeocacher);
            if($regGeocacher){
                $regGeocacher->update(
                  [
                    'nombre' => $datosExtraidos['geocacher'] ,
                    'finds' => $datosExtraidos['finds']     ,
                    'hides' => $datosExtraidos['hides']     ,
                    'favoritos' => $datosExtraidos['favoritos'] ,
                    'ciudad' => $datosExtraidos['ciudad']    ,
                  ]
                );
              } else {
                $regGeocacher = New Geocacher;
                $geocacherCreado = $regGeocacher->create(
                  [
                    'nombre' => $datosExtraidos['geocacher'] ,
                    'alias' => $datosExtraidos['geocacher'] ,
                    'finds' => $datosExtraidos['finds']     ,
                    'hides' => $datosExtraidos['hides']     ,
                    'favoritos' => $datosExtraidos['favoritos'] ,
                    'ciudad' => $datosExtraidos['ciudad']    ,
                  ]
                );

              }

              $usuario = $this->creaUserSiNoExiste($geocacher);
              $geocacherCreado->user_id = $usuario['user_id'];
              $geocacherCreado->save();
              return $usuario;
}

function creaUserSiNoExiste($geocacher){
  $usuario = User::where('username','=',$geocacher)->first();
  if($usuario){
    // Ya estas dado de alta. Vete al login y escribe tu contraseña
    $page_title = "Login";

    return view('auth.logingeocodigodavinci', compact('page_title'));
  } else {
    // No existe en USERS. Hay que crearlo.
    //$nuevoUsuario = New User;
    $nuevoUsuario = [
      'username' => $geocacher,
      'first_name' => $geocacher,
      'last_name' => $geocacher,
      'email' => $geocacher.'@geoibonito.com',
      'password' => '1111', //$this->randomPassword(),
      'enabled' => '1',
      'auth_type' => 'internal',
    ];
    $usuario = User::createGeocacher($nuevoUsuario);
    return ['password' => $nuevoUsuario['password'],'user_id' => $usuario['id']] ;
}


}

function enviaEmail($client="",$texto="",$geocacher=""){
//dd('no enviar');
            // Se las envío por email
            $urlEmail           = $this->urlDeGeocaching('email',$geocacher);
            $crawlerEmail       = $this->miCliente->request('GET', $urlEmail);//dd($crawlerEmail);
            $form = $crawlerEmail->selectButton('ctl00$ContentBody$SendMessagePanel1$btnSend')->form();
            $crawler = $client->submit($form, array('ctl00$ContentBody$SendMessagePanel1$tbMessage' => $texto));

}


function randomPassword() {
  $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
  $pass = array(); //remember to declare $pass as an array
  $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
  for ($i = 0; $i < 5; $i++) {
      $n = rand(0, $alphaLength);
      $pass[] = $alphabet[$n];
  }
  return implode($pass); //turn the array into a string
}


}

