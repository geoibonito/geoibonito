<?php namespace app\Models;

use App\Traits\BaseModelTrait;
use App\User;
use Tylercd100\LERN\Models\ExceptionModel;
use App\Models\Online;
use Illuminate\Database\Eloquent\Model;

class solucionesxgeocacher extends  Model
{
    use BaseModelTrait;

    //protected $fillable = ['user_id', 'nombre', 'ultimo_acceso', 'modulo'];
    protected $table = 'solucionesxgeocacher';
    public $timestamps = true;
    public $primaryKey = "id";
    public $incrementing = TRUE;
    public $guarded = [];

//    /**
//     * @return string
//     */
//    public function getTraceAttribute()
//    {
//        return $this->Label;
//    }

}
