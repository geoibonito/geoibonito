<?php namespace App\Models;

use App\Traits\BaseModelTrait;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    use BaseModelTrait;

    /**
     * @var array
     */
    //protected $fillable = ['user_id', 'category', 'message', 'data', 'data_parser', 'replay_route'];
    protected $table = "logs";
    protected $guarded = [];
    public $timestamps = FALSE;

}
