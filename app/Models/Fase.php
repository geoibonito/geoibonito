<?php namespace app\Models;

use App\Traits\BaseModelTrait;
use App\User;
use Tylercd100\LERN\Models\ExceptionModel;
use App\Models\Online;
use Illuminate\Database\Eloquent\Model;

class Fase extends  Model
{
    use BaseModelTrait;

    //protected $fillable = ['user_id', 'nombre', 'ultimo_acceso', 'modulo'];
    protected $table = 'fases';
    public $timestamps = true;
    public $primaryKey = "id";
    public $incrementing = TRUE;
    public $guarded = [];

    public function tareasxfase()
    {
        return $this->hasMany('App\Models\tareasxfase');
    }

}
